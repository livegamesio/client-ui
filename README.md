# LiveGames Client-UI SDK

## +Version
1.0.1

## +Başlangıç
Bu proje temel livegames.io oyuncu arayüzünü barındırmaktadır.  
Proje ile gelecek olan html ve scss dosyalarını düzenleyerek kendinize özgü yeni tasarımlar oluşturabilirsiniz.  
Ayrıca bu projeyi kullanmadan da varolan livegames.io teması ile altyapıyı kullanabilirsiniz.  

> Tüm değişiklikler bu repository üzerinden yapılacaktır.  
> Lütfen güncellemeleri takip ediniz.

## +Kurulum
Proje clone'lanmadan önce aşağıdaki paketlerin global olarak kurulduğunden emin olunuz:
  - node/npm
  - grunt
  - bower
  - node-sass (gem gerekebilir)


```sh
$ git clone [git-repo-url]
$ cd client-ui
$ npm i -d
```
Aşağıdaki seçeneklerden size uygun olanıyla css dosyalarını oluşturabilir yada monitor kullanabilirsiniz.   

app.css ve app.min.css dosyalarını oluşturur.
```sh
$ grunt build
_yada_
$ grunt build-all

```
app.css dosyasını oluşturur.
```sh
$ grunt build-sass
```

app.min.css dosyasını oluşturur.
```sh
$ grunt build-min
```

proje klasöründe değişiklik algıladığında build'i çalıştırır.
```sh
$ grunt monitor
```

  

## +Kullanım
Client projesi iki yöntemle çalışmaktadır.
* **[LoginBased]** - Her kullanıcı için sağlanacak kullanıcı adı / şifre ile giriş yapılabilir.
* **[TokenBased]** - Sizin websitenize uygun **client-api** ile token oluşturarak güvenilir sorgular yapabilirsiniz.


### ~client-api-php ile

Detaylı bilgi için **client-api-php** repository'sini kontrol edebilirsiniz.

```php
<?php
require_once("Api.php");
$user = [
	'id' => "abc1234",  //zorunlu
	'parent' => 'ResellerAccountUserId', //zorunlu
	'name' => "Name", //zorunlu (görünecek isim)
	'surname' => "Surname", //Opsiyonel
	'phone' => "05555555555", //Opsiyonel
	'email' => "user@mail.com", //Opsiyonel - LoginBased sistemler için
	'password' => "123456" //Opsiyonel - LoginBased sistemler için
];
$api = new \LiveGames\ClientSDK\Api($user, "API_KEY", "CLIENT_KEY", "CLIENT_SECRET");
?>
...
<head>
<!-- Oluşturduğunuz app.css dosyası -->
<link href="css/app.css" rel="stylesheet">
<!-- veya livegames temasını kullanabilirsiniz. -->
<link href="//embed.livegames.io/css/app.css" rel="stylesheet">
...
</head>
```
Sayfanızın içinde uygun bir alana (en az 1140px genişliğinde olacak şekilde) aşağıdaki gibi **live-game** tag'ini yerleştirebilirsiniz.

Livegames.io temasını kullanacaksanız:
```php
    <live-game sign="<?=$api->token?>" game="tombala"></live-game>
```

yada özelleştirilmiş tasarımları uyarlamak için:
```phtml
<live-game 
        tplRoom="templates/tplRoom.html"
        tplCard="templates/tplCard.html"
        tplChat="templates/tplChat.html"
        tplError="templates/tplError.html"
        tplExtraAutobuy="templates/tplExtraAutobuy.html"
        tplExtraAutoselect="templates/tplExtraAutoselect.html"
		tplLobby="templates/tplLobby.html"
        tplLogin="templates/tplLogin.html"
        tplNotifications="templates/tplNotifications.html"
        tplSaleCards="templates/tplSaleCards.html"
        tplWinnerModal="templates/tplWinnerModal.html"
        tplWinners="templates/tplWinners.html"
		tplPromptModal="templates/tplPromptModal.html"
	    sign="<?=$api->token?>"
	    game="tombala"></live-game>
```

Son olarak
```html
    ...
    <script src="//embed.livegames.com/livegames.js"></script>
</body>
</html>
```

**live-game** tag'i içinde verilen özellikler:
 * **tplTEMPLATE :** İlgili bölümün tasarımlarını içerir. Belirtilmediği taktirde **livegames.io** teması uygulanır.
 * **sign :** Özel anahtar
 * **game :** İstenilen oyun servisi

> Kullanıcıya **live-game** tag'ini göstermeden önce, kullanıcının **client-api** ile yaratılmış olması gerekmektedir. Aksi taktirde gönderilen anahtar geçersiz olacaktır.


License
----

MIT


**i can fly!**
