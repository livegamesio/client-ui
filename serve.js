var express = require('express'),
	app = express(),
	path = require('path');

app.use(express.static('./'));
app.get('/', function (req, res) {
	res.sendFile(path.join(__dirname + '/ux-engine/example/index-eif.html'));
});
app.get('/game', function (req, res) {
	res.sendFile(path.join(__dirname + '/ux-engine/example/index.html'));
});
app.listen(3000, function () {
	console.log('Running: http://localhost:3000/?test=LiveDebug');
});
