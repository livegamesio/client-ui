'use strict';

var config = {};

config.project = {
    name : 'LiveGames'
};

config.paths = {
    css:       'css/',
    html:      'templates/',
    img:       'images/',
    vendor:    'vendor/',
    images:    'ux-engine/images/',
    scss:      'ux-engine/scss/',
    templates: 'ux-engine/ui-templates/'
};

config.banner = [
    '/*! LiveGames UI \n' +
    ' *  <%= package.name %> - <%= package.author %> \n' +
    ' *  <%= package.description %> \n' +
    ' *  @build ' + new Date() + '\n' +
    ' */\n'
].join('');

module.exports = config;
