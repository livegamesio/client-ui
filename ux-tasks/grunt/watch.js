module.exports = {
    css      : {
        files: ['<%= paths.scss %>**/*.scss'],
        tasks: ['sass:expanded', 'sass:min']
    },
    templates: {
        files: ['<%= paths.templates %>/**/*.html'],
        tasks: ['clean:html', 'htmlmin']
    },
    html: {
        files: ['<%= paths.templates %>/**/*.html'],
        tasks: ['html']
    },
    images   : {
        files: ['<%= paths.images %>**/*.{png,jpg,gif}'],
        tasks: ['clean:images', 'imagemin'],
        options: {
            spawn: false
        }
    }
};