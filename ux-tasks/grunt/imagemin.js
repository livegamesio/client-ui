module.exports = {
    dist: {
        files: [{
            expand: true,
            optimizationLevel: 4,
            cwd: '<%= paths.images %>',
            src: ['**/*.{png,jpg,gif}'],
            dest: '<%= paths.img %>'
        }]
    }
};