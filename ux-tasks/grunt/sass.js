module.exports = {
    expanded: { // Target
        options: { // Target options
            style: 'expanded'
        },
        files: {
            '<%= paths.css %>v2/app.css': '<%= paths.scss %>v2/app.scss',
			'<%= paths.css %>v2/newyear.css': '<%= paths.scss %>v2/newyear.scss'
        }
    },
    min: {
        options: {
            style: 'compressed'
        },
        files: {
            '<%= paths.css %>v2/app.min.css': '<%= paths.scss %>v2/app.scss',
			'<%= paths.css %>v2/newyear.min.css': '<%= paths.scss %>v2/newyear.scss'
        }
    }
};
