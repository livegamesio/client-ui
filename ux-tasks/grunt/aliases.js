module.exports = {
    build  : {
        description: 'Default Build tasks.',
        tasks      : ['clean:html', 'clean:images', 'htmlmin', 'imagemin', 'sass:expanded', 'sass:min', 'notify']
    },
    html  : {
        description: 'Default HTML.',
        tasks      : ['clean:html', 'htmlmin']
    },
    images  : {
        description: 'Default Images.',
        tasks      : ['clean:images', 'imagemin']
    }
};
