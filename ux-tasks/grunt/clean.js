module.exports = {
    html : {
        force : true,
        src: ['<%= paths.html %>']
    },
    images : {
        force : true,
        src: ['<%= paths.img %>']
    }
};