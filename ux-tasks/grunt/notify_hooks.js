module.exports = {
    options: {
        max_jshint_notifications: 5,
        title                   : "<%= package.author %>",
        success                 : true,
        duration                : 5
    }
};