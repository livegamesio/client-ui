module.exports = {
    templates: {
        options: {
            removeComments: true,
            collapseWhitespace: true
        },
        expand: true,
        cwd: '<%= paths.templates %>',
        src: ['**/*.html'],
        dest: '<%= paths.html %>'
    }
};