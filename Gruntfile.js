/**
 * LiveGames.io Grunt File
 *
 * @since v1.0.0
 */

module.exports = function(grunt) {
	var path = require('path'),
		config = require('./ux-tasks/config');

	require('load-grunt-config')(grunt, {
		configPath: path.join(process.cwd(), './ux-tasks/grunt'),
		data: config
	});
};