<?php
require_once("Api.php");

$user = [
	'id' => "abc1234",  //zorunlu
	'parent' => 'ResellerAccountUserId', //zorunlu
	'name' => "Name", //zorunlu (görünecek isim)
	'surname' => "Surname", //Opsiyonel
	'phone' => "05555555555", //Opsiyonel
	'email' => "user@mail.com", //Opsiyonel - LoginBased sistemler için
	'password' => "123456" //Opsiyonel - LoginBased sistemler için
];

$api = new \LiveGames\ClientApi\Api($user, "CLIENT_KEY", "API_KEY", "API_SECRET");
?>
<!DOCTYPE html>
<html lang="en" ng-app="clientweb">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title>LiveGames!</title>
	<meta name="keywords" content="livegames, livegames.io">
	<meta name="description" content="Provided by LiveGames.io">

	<link href="css/app.css" rel="stylesheet">
	<!-- <link href="//ui.livegames.io/css/app.css" rel="stylesheet">-->
</head>
<body style="background-color: #666">

<div class="container">

	<live-game
		tplRoom="templates/tplRoom.html"
		tplCard="templates/tplCard.html"
		tplChat="templates/tplChat.html"
		tplError="templates/tplError.html"
		tplExtraAutobuy="templates/tplExtraAutobuy.html"
		tplExtraAutoselect="templates/tplExtraAutoselect.html"
		tplLobby="templates/tplLobby.html"
		tplLogin="templates/tplLogin.html"
		tplNotifications="templates/tplNotifications.html"
		tplSaleCards="templates/tplSaleCards.html"
		tplWinnerModal="templates/tplWinnerModal.html"
		tplWinners="templates/tplWinners.html"
		tplPromptModal="templates/tplPromptModal.html"
		sign="<?= $api->token ?>"
		game="tombala"></live-game>

</div>

<script src="//embed.livegames.io/livegames.js"></script>
</body>
</html>